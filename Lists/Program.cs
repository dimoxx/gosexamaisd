﻿using System;
using Lists.Lists;

namespace Lists {
    internal static class Program {
        private static void Main(string[] args) {
            var dl = new DoublyLinkedList<int>();
            dl.AddFirst(3);
            dl.AddFirst(2);
            dl.AddLast(4);
            dl.AddLast(5);
            dl.AddBefore(1, dl.Find(2));
            dl.AddAfter(6, dl.Find(5));
            dl.Remove(1);
            dl.Remove(4);
            dl.Remove(6);
            dl.RemoveHead();
            dl.RemoveLast();
            dl.Remove(dl.Find(3));
            dl.AddLast(7);
            Console.WriteLine(dl.ToString(' '));
            
            var cl = new CircularLinkedList<int>();
            cl.AddFirst(3);
            cl.AddFirst(2);
            cl.AddLast(4);
            cl.AddLast(5);
            cl.AddBefore(1, cl.Find(2));
            cl.AddAfter(6, cl.FindLast(5));
            cl.Remove(1);
            cl.Remove(4);
            cl.Remove(6);
            cl.RemoveHead();
            cl.RemoveLast();
            cl.Remove(cl.Find(3));
            cl.AddLast(7);
            Console.WriteLine(cl.ToString(' '));
        }
    }
}
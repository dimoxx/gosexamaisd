﻿using System;
using System.Text;

namespace Lists.Lists {
    public class CircularLinkedList<T> where T : IComparable<T> {
        private ListNode<T> _head;

        /// <summary>
        /// Возвращает ссылку на первый элемент со значением value
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public ListNode<T> Find(T value) {
            var ptrHead = _head;
            if (value == null || ptrHead == null) {
                return null;
            }
            do {
                if (ptrHead.Value.CompareTo(value) == 0) {
                    return ptrHead;
                }
                ptrHead = ptrHead.Next;
            } while (!ReferenceEquals(ptrHead, _head));
            return null;
        }

        /// <summary>
        /// Тоже самое, но обход с конца.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public ListNode<T> FindLast(T value) {
            var ptrLast = _head.Prev;
            if (value == null || ptrLast == null) {
                return null;
            }
            do {
                if (ptrLast.Value.CompareTo(value) == 0) {
                    return ptrLast;
                }

                ptrLast = ptrLast.Prev;
            } while (!ReferenceEquals(ptrLast, _head.Prev));
            return null;
        }

        public void AddFirst(T value) {
            if (value == null) {
                throw new ArgumentNullException(nameof(value));
            }
            var item = new ListNode<T> {
                Value = value,
            };
            if (_head == null) {
                CreateHead(item);
                return;
            }
            InsertBefore(_head, item);
            _head = item;
        }

        public void AddLast(T value) {
            if (value == null) {
                throw new ArgumentNullException(nameof(value));
            }
            var item = new ListNode<T> {
                Value = value,
            };
            if (_head == null) {
                CreateHead(item);
                return;
            }
            InsertBefore(_head, item);
        }

        public void AddAfter(T value, ListNode<T> node) {
            if (value == null) {
                throw new ArgumentNullException(nameof(value));
            }
            var item = new ListNode<T> {
                Value = value
            };
            InsertBefore(node.Next, item);
        }

        public void AddBefore(T value, ListNode<T> node) {
            if (value == null) {
                throw new ArgumentNullException(nameof(value));
            }
            var item = new ListNode<T> {
                Value = value
            };
            InsertBefore(node, item);
            if ( node == _head) {
                _head = item;
            }
        }

        public void Remove(T value) {
            Remove(Find(value));
        }

        public void RemoveHead() {
            Remove(_head);
        }

        public void RemoveLast() {
            Remove(_head.Prev);
        }

        public void Remove(ListNode<T> node) {
            //Если только 1 элемент в списке
            if (node.Next == node) {
                _head = null;
            }
            //Перед элементом, идущим за node будет тот, что был перед node
            node.Next.Prev = node.Prev;
            //После элемента, идущего перед node будет тот, что был за node
            node.Prev.Next = node.Next;
            //Если удаляем голову списка, то головой списка становится элемент после неё.
            if (_head == node) {
                _head = node.Next;
            }
        }

        public string ToString(char separator) {
            var sb = new StringBuilder();
            var headPtr = _head;
            do {
                sb.Append(headPtr.Value.ToString() + separator);
            } while (!ReferenceEquals(_head, headPtr));
            return sb.ToString();
        }

        /// <summary>
        /// Вставка перед элементом
        /// </summary>
        /// <param name="node">элемент, перед которым вставляем</param>
        /// <param name="addedNode">элемент, который вставляем</param>
        private void InsertBefore(ListNode<T> node, ListNode<T> addedNode) {
            //перед новым элементом будет тот, что был перед node
            addedNode.Prev = node.Prev;
            //после него будет node
            addedNode.Next = node;
            //после элемента, бывшего перед node будет новый элемент
            node.Prev.Next = addedNode;
            //перед node будет новый элемент.
            node.Prev = addedNode;
        }

        private void CreateHead(ListNode<T> node) {
            node.Next = node;
            node.Prev = node;
            _head = node;
        }
    }
}
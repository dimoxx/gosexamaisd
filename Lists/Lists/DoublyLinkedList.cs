﻿using System;
using System.Text;

namespace Lists.Lists {
    public class DoublyLinkedList<T> where T: IComparable<T> {
        private ListNode<T> _head;
        private ListNode<T> _last;
        /// <summary>
        /// Возвращает ссылку на первый элемент со значением value
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public ListNode<T> Find(T value) {
            if (value == null) {
                return null;
            }
            //Поиск одновременно в двух направлениях
            var ptrHead = _head;
            var ptrLast = _last;
            //Пока указатели не указывают на один и тот же элемент
            //(для null, null referenceequals вернет true)
            while (!ReferenceEquals(ptrHead, ptrLast)) {
                if (ptrHead != null) {
                    if (value.CompareTo(ptrHead.Value) == 0) {
                        return ptrHead;
                    }
                    ptrHead = ptrHead.Next;
                }
                if (ptrLast != null) {
                    if (value.CompareTo(ptrLast.Value) == 0) {
                        return ptrLast;
                    }
                    ptrLast = ptrLast.Prev;
                }
            }
            if (ptrHead != null && value.CompareTo(ptrHead.Value) == 0) {
                return ptrHead;
            }
            return null;
        }
        
        public void AddFirst(T value) {
            if (value == null) {
                throw new ArgumentNullException(nameof(value));
            }
            var item = new ListNode<T> {
                Value = value,
            };
            InsertBefore(_head, item);
        }
        
        public void AddLast(T value) {
            if (value == null) {
                throw new ArgumentNullException(nameof(value));
            }
            var item = new ListNode<T> {
                Value = value,
            };
            InsertBefore(item, _last);
        }

        public void AddAfter(T value, ListNode<T> node) {
            if (value == null) {
                throw new ArgumentNullException(nameof(value));
            }
            var item = new ListNode<T> {
                Value = value
            };
            InsertBefore(item, node);
        }

        public void AddBefore(T value, ListNode<T> node) {
            if (value == null) {
                throw new ArgumentNullException(nameof(value));
            }
            var item = new ListNode<T> {
                Value = value
            };
            InsertBefore(node, item);
        }

        public void Remove(T value) {
            Remove(Find(value));
        }

        public void RemoveHead() {
            Remove(_head);
        }

        public void RemoveLast() {
            Remove(_last);
        }
        
        public void Remove(ListNode<T> node) {
            var hasPrev = !ReferenceEquals(node.Prev, null);
            var hasNext = !ReferenceEquals(node.Next, null);
            if (node == null) {
                throw new ArgumentNullException(nameof(node));
            }
            if (node == _head) {
                _head = node.Next;
                if (_head != null) {
                    _head.Prev = null;
                }
            }
            if (node == _last) {
                _last = node.Prev;
                if (_last != null) {
                    _last.Next = null;
                }
            }
            if (hasPrev) {
                node.Prev.Next = node.Next;
            }
            if (hasNext) {
                node.Next.Prev = node.Prev;
            }
        }

        public string ToString(char separator) {
            var sb = new StringBuilder();
            for (var i = _head; i != null; i = i.Next) {
                sb.Append(i.Value.ToString() + separator);
            }
            return sb.ToString();
        }
        
        /// <summary>
        /// Вставка перед элементом
        /// </summary>
        /// <param name="node">элемент, перед которым вставляем</param>
        /// <param name="addedNode">элемент, который вставляем</param>
        private void InsertBefore(ListNode<T> node, ListNode<T> addedNode) {
            //Если в списке нет элементов
            if (_head == null) {
                _head = addedNode ?? node;
                _last = _head;
                return;
            }
            //перед новым элементом будет тот, что был перед node
            var hasPrev = !ReferenceEquals(node.Prev, null);
            if (hasPrev) {
                addedNode.Prev = node.Prev;
            }
            //после него будет node
            addedNode.Next = node;
            //после элемента, бывшего перед node будет новый элемент
            if (hasPrev) {
                node.Prev.Next = addedNode;
            }
            //перед node будет новый элемент.
            node.Prev = addedNode;

            //Обновляем head и last при необходимости.
            if (addedNode.Prev == null) {
                _head = addedNode;
            }
            if (node.Next == null) {
                _last = node;
            }
        }
    }

    public class ListNode<T> where T : IComparable<T> {
        public T Value { get; set; }
        public ListNode<T> Prev { get; set; }
        public ListNode<T> Next { get; set; }
    } 
}
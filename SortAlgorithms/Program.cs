﻿using System;
using System.Diagnostics;
using SortAlgorithms.Algorithms;

namespace SortAlgorithms {
    internal static class Program {
        
        private static readonly Random R;
        private static readonly int Size;
        private static readonly Stopwatch Sw;
        
        static Program() {
            R = new Random(Guid.NewGuid().GetHashCode());
            Size = 2000000;
            Sw = new Stopwatch();
        }

        private static void Main(string[] args) {
            var matrix = CreateMatrix(8, Size);
            Sw.Start();
            Console.WriteLine("Counting Universal");
            CountingSort.Sort(matrix[0]);
            Sw.Stop();
            Console.WriteLine($"Counting Universal Sorting time {Sw.ElapsedMilliseconds} ms.");
            Console.WriteLine($"Counting Universal Sorting Correct? {(IsSorted(matrix[0]) ? "Yes" : "No")}");
           
            Sw.Restart();
            Console.WriteLine("Counting Integer");
            CountingSort.IntegerSort(matrix[1]);
            Sw.Stop();
            Console.WriteLine($"Counting Integer Sorting time {Sw.ElapsedMilliseconds} ms.");
            Console.WriteLine($"Counting Integer Sorted Correct? {(IsSorted(matrix[1]) ? "Yes" : "No")}");
            
            Sw.Restart();
            Console.WriteLine("Merge");
            MergeSort.Sort(matrix[2]);
            Sw.Stop();
            Console.WriteLine($"Merge Sorting time {Sw.ElapsedMilliseconds} ms.");
            Console.WriteLine($"Merge Sorted Correct? {(IsSorted(matrix[2]) ? "Yes" : "No")}");
            
            Sw.Restart();
            Console.WriteLine("Quick");
            QuickSort.Sort(matrix[3]);
            Sw.Stop();
            Console.WriteLine($"Quick Sorting time {Sw.ElapsedMilliseconds} ms.");
            Console.WriteLine($"Quick Sorted Correct? {(IsSorted(matrix[3]) ? "Yes" : "No")}");
            
            Sw.Restart();
            Console.WriteLine("Radix Digit");
            RadixSort.DigitsSort(matrix[4]);
            Sw.Stop();
            Console.WriteLine($"Radix Digit Sorting time {Sw.ElapsedMilliseconds} ms.");
            Console.WriteLine($"Radix Digit Sorted Correct? {(IsSorted(matrix[4]) ? "Yes" : "No")}");
            
            Sw.Restart();;
            Console.WriteLine("Radix Bits");
            RadixSort.BitsSort(matrix[5]);
            Sw.Stop();
            Console.WriteLine($"Radix Bits Sorting time {Sw.ElapsedMilliseconds} ms.");
            Console.WriteLine($"Radix Bits Sorted Correct? {(IsSorted(matrix[5]) ? "Yes" : "No")}");
            
            Sw.Restart();;
            Console.WriteLine("Radix Bits Improved");
            RadixSort.ImprovedBitsSort(matrix[6]);
            Sw.Stop();
            Console.WriteLine($"Radix Bits Improved Sorting time {Sw.ElapsedMilliseconds} ms.");
            Console.WriteLine($"Radix Bits Improved Sorted Correct? {(IsSorted(matrix[6]) ? "Yes" : "No")}");
            
            Sw.Restart();;
            Console.WriteLine("Standart");
            Array.Sort(matrix[7]);
            Sw.Stop();
            Console.WriteLine($"Standart Sorting time {Sw.ElapsedMilliseconds} ms.");
            Console.WriteLine($"Standart Sorted Correct? {(IsSorted(matrix[7]) ? "Yes" : "No")}");

            var gnomeSortArray = GenerateArray(20000);
            Sw.Restart();;
            Console.WriteLine("Gnome");
            GnomeSort.Sort(gnomeSortArray);
            Sw.Stop();
            Console.WriteLine($"Gnome Sorting time {Sw.ElapsedMilliseconds} ms.");
            Console.WriteLine($"Gnome Sorted Correct? {(IsSorted(gnomeSortArray) ? "Yes" : "No")}");
            
            Console.ReadKey();
        }

        private static int[] GenerateArray(int size) {
            var res = new int[size];
            for (var i = 0; i < res.Length; ++i) {
                res[i] = R.Next(0, 10000);
            }
            return res;
        }

        private static int[][] CreateMatrix(int rows, int columns) {
            var matrix = new int[rows][];
            for (var i = 0; i < rows; ++i) {
                if (i == 0) {
                    matrix[0] = GenerateArray(columns);
                }
                else {
                    matrix[i] = new int[columns];
                    matrix[0].CopyTo(matrix[i], 0);
                }
            }
            return matrix;
        }

        private static bool IsSorted(int[] arr) {
            var res = true;
            for (var i = 0; i < arr.Length - 1; ++i) {
                res &= arr[i] <= arr[i + 1];
                if (!res) {
                    break;
                }
            }
            return res;
        }
    }
}
﻿using System;
using SortAlgorithms.Helpers;

namespace SortAlgorithms.Algorithms {
    public static class GnomeSort {
        /// <summary>
        /// Сложность O(n^2)
        /// </summary>
        /// <param name="arr"></param>
        /// <typeparam name="T"></typeparam>
        public static void Sort<T>(T[] arr) where T : struct, IComparable<T> {
            var i = 1;
            //Пока не дошли до конца массива
            while (i < arr.Length) {
                //Если дошли до начала массива или предыдущий элемент меньше  текущего
                //(пара элементов отсортирована), переходим к следующей паре элементов
                if (i == 0 || arr[i - 1].CompareTo(arr[i]) <= 0) {
                    ++i;
                }
                //Меняем порядок элементов и возвращаемся к предыдущей паре.
                else {
                    SwapHelper.Swap(ref arr[i], ref arr[i - 1]);
                    --i;
                }
            }
        }
    }
}
﻿using System;
using SortAlgorithms.Helpers;

namespace SortAlgorithms.Algorithms {
    public static class QuickSort {

        
        public static void Sort<T>(T[] arr) where T: struct, IComparable<T> {
            Sort(arr, 0, arr.Length - 1);
        }
        
        /// <summary>
        /// Сложность O(n * log(n))
        /// </summary>
        /// <param name="arr">массив</param>
        /// <param name="l">Левая граница подотрезка</param>
        /// <param name="r">Правая граница подотрезка</param>
        /// <typeparam name="T"></typeparam>
        private static void Sort<T>(T[] arr, long l, long r) where T : struct, IComparable<T> {
            //Если левая граница совпадает с правой, выход
            if (l >= r) {
                return;
            }
            var left = l;
            var right = r;
            //Индекс середины(почти) подотрезка (опорный элемент)
            var middle = (left >> 1) + (right >> 1);
            //значения <= arr[middle] должны быть левее middle, >= arr[middle] - правее.
            do {
                //Идем от начала подотрезка и ищем индекс элемента,
                //значение которого больше или равно значению arr[middle]
                while (arr[left].CompareTo(arr[middle]) < 0) {
                    ++left;
                }
                //Идем c конца подотрезка и ищем индекс элемента,
                //значение которого меньше или равно значению arr[middle]
                while (arr[right].CompareTo(arr[middle]) > 0) {
                    --right;
                }
                //Меняем arr[left] и arr[right] местами, тк arr[left] >= arr[middle] и arr[right] <= arr[middle]
                if (left <= right) {
                    SwapHelper.Swap(ref arr[left], ref arr[right]);
                    left++;
                    right--;
                }
            } while (left <= right);
            //После этой процедуры right < left
            //Выполняем тоже самое для левой половины подотрезка
            Sort(arr, l, right);
            //И для правой
            Sort(arr, left, r);
        }
    }
}
﻿using System;

namespace SortAlgorithms.Algorithms {
    public static class MergeSort {

        public static void Sort<T>(T[] arr) where T : struct, IComparable<T> {
            Sort(arr, 0, arr.Length - 1);
        }
        /// <summary>
        /// Сложность O(n * log(n))
        /// </summary>
        /// <param name="arr">Массив</param>
        /// <param name="l">Индекс левой границы</param>
        /// <param name="r">Индекс правой границы</param>
        /// <typeparam name="T"></typeparam>
        private static void Sort<T>(T[] arr, long l, long r) where T : struct, IComparable<T> {
            //Если левая граница совпадает с правой, выход
            if (l >= r) {
                return;
            }
            //Индекс середины подотрезка массива (ну, почти середины)
            var middle = (l >> 1) + (r >> 1);
            //Сортируем левую половину подотрезка
            Sort(arr, l, middle);
            //Сортируем правую половину подотрезка
            Sort(arr, middle + 1, r);
            //Выполняем слияние отсортированных половин
            Merge(arr, l, middle, r);
        }

        /// <summary>
        /// Слияние
        /// </summary>
        /// <param name="arr">Массив</param>
        /// <param name="l">Левая граница</param>
        /// <param name="middle">Середина подотрезка</param>
        /// <param name="r">Правая граница</param>
        /// <typeparam name="T"></typeparam>
        private static void Merge<T>(T[] arr, long l, long middle, long r) where T : struct, IComparable<T> {
            //Вспомогательный массив
            var tmp = new T[r - l + 1];
            var tmpInd = 0L;
            var left = l;
            var right = middle + 1;
            //Пока не дошли до конца одной из половин подотрезка
            while ((left <= middle) && (right <= r)) {
                //Если текущий элемент из левойполовины меньше текущего из правой
                //Записываем в в вспомогательный массив текущий элемент из левой половины
                //Переходим к следующему.
                if (arr[left].CompareTo(arr[right]) < 0) {
                    tmp[tmpInd] = arr[left];
                    ++left;
                }
                //Тоже самое для правой половины
                else {
                    tmp[tmpInd] = arr[right];
                    ++right;
                }
                ++tmpInd;
            }
            //Если в левой половине есть элементы, большие последнего элемента правой половины
            //Записываем их в конец вспомогательного массива
            if (left <= middle) {
                while (left <= middle) {
                    tmp[tmpInd] = arr[left];
                    ++left;
                    ++tmpInd;
                }
            }
            //Тоже самое для правой половины
            if (right <= r) {
                while (right <= r) {
                    tmp[tmpInd] = arr[right];
                    ++right;
                    ++tmpInd;
                }
            }           
            //tmp содержит элементы из подотрезка [l;r], отсортированные по возрастанию
            //перезаписываем значения элементов из подотрезка [l;r] исходного массива.
            for (var i = 0; i < tmp.Length; ++i) {
                arr[l + i] = tmp[i];
            }
        }
    }
}
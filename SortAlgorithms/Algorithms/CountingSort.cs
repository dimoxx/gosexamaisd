﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SortAlgorithms.Algorithms {
    public static class CountingSort {
        
        /// <summary>
        /// Вариант, когда:
        /// 1)неизвестен диапазон сортируемых чисел
        /// 2)сортируются вещественные числа
        /// 3)сортируются не числа
        /// Сложность O(n + k * log(k)) n - кол-во элементов, k - кол-во уникальных элементов
        /// при k существенно меньшем n ~ O(n)
        /// </summary>
        /// <param name="arr">входной массив</param>
        /// <typeparam name="T">структура, реализующая Icomparable</typeparam>
        public static void Sort<T>(T[] arr) where T : struct, IComparable<T> {
            var buffer = new Dictionary<T, int>();
            
            //Подсчет кол-ва вхождений каждого элемента
            foreach (var t in arr) {
                if (!buffer.ContainsKey(t)) {
                    buffer.Add(t, 0);
                }
                ++buffer[t];
            }
            
            //Сортировка уникальных элементов
            var sortedKeys = buffer.Keys.OrderBy(x => x);
            
            //Заполнение массива
            var currentIndex = 0L;
            foreach (var key in sortedKeys) {
                for (var i = 0; i < buffer[key]; ++i ) {
                    arr[currentIndex++] = key;
                }
            }
        }
        
        /// <summary>
        /// Для целых чисел, диапазон известен
        /// Сложность O(n)
        /// </summary>
        /// <param name="arr"></param>
        public static void IntegerSort (int[] arr) {
            //Левая граница диапазона
            var minValue = arr.Min();
            //Правая граница диапазона
            var maxValue = arr.Max();
            //Подсчет кол-ва каждого элемента
            var buffer = new long[maxValue - minValue + 1];
            foreach (var elem in arr) {
                ++buffer[elem - minValue];
            }
            //Заполнение массива.
            var currentIndex = 0;
            for (var i = 0; i < buffer.Length; ++i) {
                for (var j = 0; j < buffer[i]; ++j) {
                    arr[currentIndex++] = minValue + i;
                }
            }
        }
    }
}
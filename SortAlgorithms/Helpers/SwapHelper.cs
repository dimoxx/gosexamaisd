﻿using System;

namespace SortAlgorithms.Helpers {
    public static class SwapHelper {
        public static void Swap<T>(ref T a, ref T b) where T : struct, IComparable<T> {
            var tmp = a;
            a = b;
            b = tmp;
        }
    }
}